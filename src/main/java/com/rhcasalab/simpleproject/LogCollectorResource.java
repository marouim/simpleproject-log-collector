package com.rhcasalab.simpleproject;

import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.annotations.SseElementType;

@ApplicationScoped
@Path("/")
public class LogCollectorResource {
    
    @Inject
    @RestClient
    LogService logService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String get() {
        return "hello";
    }

    @POST
    public Response createLog(String log) {
        System.out.println("PROCESSING EVENT: " + log);
        
        
        Jsonb jsonb = JsonbBuilder.create();

		Log l = jsonb.fromJson(log, Log.class);
        System.out.println("PROCESSING message: " + l.message);
        
        logService.saveLog(l);

        try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
        }
        
        System.out.println("DONE PROCESSING EVENT: " + log);
        return Response.ok().build();
    }

    @GET
    @Path("/healthz")
    @Produces(MediaType.TEXT_PLAIN)
    public Response health() {
        return Response.ok().build();
    }
}