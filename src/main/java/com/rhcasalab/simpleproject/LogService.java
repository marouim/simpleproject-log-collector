package com.rhcasalab.simpleproject;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Set;


@RegisterRestClient
public interface LogService {

    @POST
    @Path("/v1/logs")
    @Produces("application/json")
    void saveLog(Log log);
}